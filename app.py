from flask import Flask, render_template, request
from helper import Helper

app = Flask(__name__)


@app.route("/",methods=["POST", "GET"])
def home():
    prediction = "-"
    if request.method == "POST":
        inputUser = request.form['input']
        helper = Helper()
        preprocess = helper.preprocessing(inputUser)
        ubahInput = helper.change_input(preprocess)
        hasil = helper.sequence(ubahInput)
        prediction = helper.model_predict(hasil)
        return render_template("index.html", prediction = prediction)
    else:
        return render_template("index.html", prediction = prediction)

@app.route("/deskripsi")
def deskripsi():
    return render_template("deskripsi.html")

@app.route("/kinerja",methods=["POST", "GET"])
def kinerja():
    klasifikasi = "-"
    if request.method == "POST":
        file = request.files['input-file']
        helper = Helper()
        prepare = helper.dataset(file)
        klasifikasi = helper.print_kinerja(prepare)
        return render_template("kinerja.html", klasifikasi = klasifikasi)
    else:
        return render_template("kinerja.html", klasifikasi = klasifikasi)



if(__name__) == '__main__':
    app.run(debug=True, host="localhost", port=8000)
import pandas as pd
import numpy as np
import ast
import re
# import nltk
# nltk.download('stopwords')
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
from Sastrawi.Stemmer.StemmerFactory import StemmerFactory
from keras.preprocessing.text import Tokenizer
from keras.utils import to_categorical
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
import matplotlib.pyplot as plt
import seaborn as sns
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import load_model




class Helper(object):

    def dataset(self, file):
    # Membaca file dataset
        df = pd.read_csv(file)
        X = df['content'].tolist()
        y = df['labels'].tolist()
        X_preprocessed = [self.preprocessing(data) for data in X]
        df_preprocessed = pd.DataFrame(
            {'processing_result': X_preprocessed, 'labels': y,})
        return df_preprocessed

    def print_kinerja(self, df_preprocessed):
            model = load_model("model/model-10")
            X = df_preprocessed['processing_result']
            y = df_preprocessed['labels']
            dump = self.sequence(X)
            y = to_categorical(y, 3)
            y_pred = model.predict(dump)
            y_pred = np.argmax(y_pred, axis=1)
            y = np.argmax(y, axis=1)
            cm = confusion_matrix(y, y_pred)
            labels = ['Negatif', 'Netral', 'Positif']
            sns.heatmap(cm, annot=True, cmap="Blues", xticklabels=labels, yticklabels=labels)
            plt.xlabel('Predicted')
            plt.ylabel('Actual')
            plt.title('Confusion Matrix')
            plt.show
            plt.savefig('static/img/confussion_matrix.png')
            plt.close()
            return classification_report(y, y_pred, digits=4, output_dict=True)

    def preprocessing(self,text):
        #Cleaning
        text = re.sub(r"(?:\@|http?\://|https?\://|www)\S+", "", text) # menghapus http/https (link)
        text = re.sub(r'[^\w\s]', ' ', text) # menghilangkan tanda baca
        text = re.sub('<.*?>', ' ', text) # mengganti karakter html dengan tanda petik
        text = re.sub('[\s]+', ' ', text) # menghapus spasi berlebihan
        text = re.sub('[^a-zA-Z]', ' ', text) # mempertimbangkan huruf dan angka
        text = re.sub("\n", " ", text) # mengganti line baru dengan spasi
        text = ' '.join(text.split()) # memisahkan dan menggabungkan kata
         
        #Case Folding
        text = text.lower() # mengubah ke huruf kecil

        #Tokenize
        regexp = RegexpTokenizer(r'\w+|$[0-9]+|\S+')
        text = regexp.tokenize(text)
    
        #Stopword
        list_stopword = set(stopwords.words('indonesian'))
        hapus_kata = {"tidak", "enggak"}
        list_stopword.difference_update(hapus_kata)
        text = [token for token in text if token not in list_stopword]
        
        #Stemming
        factory = StemmerFactory()
        stemmer = factory.create_stemmer()
        text = [stemmer.stem(word) for word in text]
        return text

    def sequence(self,text):
        dataset = pd.read_csv("data/hasil_prapengolahan.csv")
        dataset["processing_result"] = dataset["processing_result"].apply(lambda x: ast.literal_eval(x))
        X = dataset["processing_result"].tolist()
        tokenizer = Tokenizer()
        tokenizer.fit_on_texts(X)
        sequences = tokenizer.texts_to_sequences(X)
        max_length = max([len(s) for s in sequences])
        sequence = tokenizer.texts_to_sequences(text)
        padding = pad_sequences(sequence, maxlen=max_length)
        return padding

    def change_input(self,text):
        text = [' '.join(text)]
        return text

    def model_predict(self,input):
        model = load_model("model/model-10")
        predict = model.predict(input)
        labels = ["NEGATIF", "NETRAL", "POSITIF"]
        prediksi = labels[np.argmax(predict)]
        return prediksi